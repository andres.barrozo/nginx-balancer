#!/bin/bash

# Valido si el usuario con el que se ejecuta es root
if [ $UID -ne 0 ]; then
	echo "Ejecutar como 'root' o 'sudo ./$0'"
	exit 1
fi

# Seteo Hostname
read -p "Hostname: " host
hostnamectl set-hostname $host

# Actualizamos el Sistema
apt update && apt upgrade -y || yum update -y || dnf update -y

# Instala NGINX
apt install nginx -y || yum install nginx -y || dnf install nginx -y

